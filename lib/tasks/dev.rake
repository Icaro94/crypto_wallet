namespace :dev do
  desc "Configura o ambiente de desenvolvimento"
  task setup: :environment do
    if Rails.env.development?
      show_spinner("Apagado DB ...") {%x(rails db:drop)}
      show_spinner("Criando DB ...") {%x(rails db:create)}
      show_spinner("Migrando DB ...") {%x(rails db:migrate)}
      show_spinner("Cadastrando tipos de mineração ...") {%x(rails dev:add_mining_types)}
      show_spinner("Cadastrando moedas ...") {%x(rails dev:add_coins)}
    else
      puts "Você não está em ambiente de desenvolvimento!"
    end
  end
private
  desc "Cadastra as moedas"
  task add_coins: :environment do
    coins=[
      {      
        description:"Bitcoin",
        acronym:"BTC",
        url_image:"http://pngimg.com/uploads/bitcoin/bitcoin_PNG47.png",
        mining_type: MiningType.find_by(acronym:'PoW')  
      },
      {   
        description:"Ethereum",
        acronym:"ETH",
        url_image:"https://cdn.iconscout.com/icon/free/png-256/ethereum-3-569581.png",
        mining_type: MiningType.all.sample 
      },
      {    
        description:"Dash",
        acronym:"DASH",
        url_image:"https://media.dash.org/wp-content/uploads/dash-D-blue.png",
        mining_type: MiningType.all.sample
      },
      {    
        description:"Iota",
        acronym:"IOT",
        url_image:"https://dl.airtable.com/qU3WA8l7RNugSz7XGaNn_large_a2d0a422b38e8427adb36450d17e69f1_11156357cf86_t.png",
        mining_type: MiningType.all.sample
      },
      {    
        description:"ZCash",
        acronym:"ZEC",
        url_image:"https://z.cash/wp-content/uploads/2019/03/zcash-icon-fullcolor.png",
        mining_type: MiningType.all.sample
      }
    ]
    
    coins.each do |coin|
      Coin.find_or_create_by!(coin)
    end
  end

  desc "Cadastra os tipos de mineração"
  task add_mining_types: :environment do
    mining_types = [
      {description:"Proof of Work", acronym:"PoW"},
      {description:"Proof of Stake", acronym:"PoS"},
      {description:"Proof of Capacity", acronym:"PoC"}
    ]
    mining_types.each do |mining_type|
      MiningType.find_or_create_by!(mining_type)
    end    
  end

  def show_spinner msg_start,msg_end = "Concluído!"
    spinner=TTY::Spinner.new "[:spinner] #{msg_start}"
    spinner.auto_spin
    yield
    spinner.success "#{msg_end}"
  end
end
