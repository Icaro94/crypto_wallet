module WelcomeHelper 
  def model_helper model
    if model == :Moedas
      Coin.model_name.human
    else
      MiningType.model_name.human
    end
  end
end