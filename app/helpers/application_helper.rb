module ApplicationHelper
  def ambiente_rails
    if Rails.env.production?
      "Produção"      
    elsif Rails.env.development?
      "Desenvolvimento"  
    else
      "Teste"
    end
  end
  def locale
    I18n.locale == :en ? "Estados Unidos" : "Português do Brasil"
  end
end
